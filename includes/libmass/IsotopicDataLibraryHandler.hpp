/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

#include <limits>
#include <vector>
#include <memory>

/////////////////////// Qt includes


/////////////////////// Local includes
#include "globals.hpp"
#include "IsotopicData.hpp"
#include "IsotopicDataBaseHandler.hpp"


namespace msxps
{
namespace libmass
{


class IsotopicDataLibraryHandler : public IsotopicDataBaseHandler
{

  public:
  IsotopicDataLibraryHandler();
  IsotopicDataLibraryHandler(IsotopicDataSPtr isotopic_data_sp);
  //IsotopicDataLibraryHandler(const QString &file_name = QString());
  //IsotopicDataLibraryHandler(IsotopicDataSPtr isotopic_data_sp, const QString &file_name = QString());

  virtual ~IsotopicDataLibraryHandler();

  // Load all the isotopic data from the IsoSpec library tables. This
  // pseudo-override takes *no* file name as it does not need any.
  //using IsotopicDataBaseHandler::loadData;
  virtual std::size_t loadData(const QString &filename) override;
  virtual std::size_t loadData();

  // Write the isotopic data to a file. Here the is some leeway (file_name might
  // be empty, some alternatives will be searched for).
  virtual std::size_t writeData(const QString &file_name = QString()) override;

  protected:
  virtual std::size_t checkConsistency() override;
};

typedef std::shared_ptr<IsotopicDataLibraryHandler> IsotopicDataLibraryHandlerSPtr;
typedef std::shared_ptr<const IsotopicDataLibraryHandler>
  IsotopicDataLibraryHandlerCstSPtr;

} // namespace libmass

} // namespace msxps

