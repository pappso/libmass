/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// pappsomspp includes
#include <limits>
#include <pappsomspp/trace/trace.h>
#include <pappsomspp/trace/maptrace.h>
#include <pappsomspp/processing/combiners/massdatacombinerinterface.h>
#include <pappsomspp/processing/combiners/mzintegrationparams.h>

#include "PeakCentroid.hpp"
#include "MassPeakShaperConfig.hpp"
#include "MassPeakShaper.hpp"


/////////////////////// Local includes


namespace msxps
{

namespace libmass
{

using IsotopicClusterChargePair = std::pair<pappso::TraceCstSPtr, int>;


class IsotopicClusterShaper
{
  public:
  // Version where only one isotopic cluster (centroid data) is to be
  // processed.
  IsotopicClusterShaper(const pappso::Trace &isotopic_cluster,
                        int charge,
                        const libmass::MassPeakShaperConfig &config);

  IsotopicClusterShaper(
    const std::vector<IsotopicClusterChargePair> &isotopic_cluster_charge_pairs,
    const libmass::MassPeakShaperConfig &config);

  virtual ~IsotopicClusterShaper();

  // Single isotopic cluster version. Reset automatically the shaped isotopic
  // clusters.
  void setIsotopicCluster(const pappso::Trace &isotopic_cluster, int charge);
  void setIsotopicCluster(pappso::TraceCstSPtr isotopic_cluster_sp, int charge);
  void
  setIsotopicCluster(IsotopicClusterChargePair isotopic_cluster_charge_pair);

  // Multiple isotopic clusters version. Reset automatically the shaped isotopic
  // clusters.
  void
  setIsotopicClusterChargePairs(const std::vector<IsotopicClusterChargePair>
                                  &isotopic_cluster_charge_pairs);

  void appendIsotopicCluster(const pappso::Trace &isotopic_cluster, int charge);
  void
  appendIsotopicClusterChargePairs(const std::vector<IsotopicClusterChargePair>
                                     &isotopic_cluster_charge_pairs);

  void setConfig(libmass::MassPeakShaperConfig config);
  libmass::MassPeakShaperConfig getConfig() const;

  // The normalizing intensity usually is the greatest intensity in the cluster
  // centroids that is to be used for normalization of the peaks in the cluster.
  void setNormalizeIntensity(int new_max_intensity);
  int getNormalizeIntensity() const;

  pappso::Trace &run(bool reset = true);

  QString shapeToString();

  private:
  // This is the starting material for the shaping. There can be as many
  // isotopic clusters (centroid data) in the vector as necessary. All these
  // clusters are going to be processed and the resulting shaped isotopic
  // cluster data are combined into the member MapTrace.

  std::vector<IsotopicClusterChargePair> m_isotopicClusterChargePairs;

  libmass::MassPeakShaperConfig m_config;

  pappso::MapTrace m_mapTrace;
  pappso::Trace m_finalTrace;
  pappso::MzIntegrationParams m_mzIntegrationParams;

  // Of all the peak centroids' intensities, what is the m/z value of the most
  // intense?
  double m_mostIntensePeakMz = 0.0;

  double m_smallestMz = std::numeric_limits<double>::max();
  double m_greatestMz = std::numeric_limits<double>::min();

  int m_normalizeIntensity = 1;

  void setIsotopicCluster(pappso::TraceCstSPtr isotopic_cluster_sp,
                          int charge,
                          bool reset);

  void setIsotopicCluster(const pappso::Trace &isotopic_cluster,
                          int charge,
                          bool reset);

  void
  setIsotopicCluster(IsotopicClusterChargePair isotopic_cluster_charge_pair,
                     bool reset);

  void setIsotopicClusterChargePairs(
    const std::vector<IsotopicClusterChargePair> &isotopic_cluster_charge_pairs,
    bool reset);
};

} // namespace libmass

} // namespace msxps

