/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QString>
#include <QDebug>
#include <QDomElement>


/////////////////////// Local includes
#include "Formula.hpp"
#include "IsotopicData.hpp"


namespace msxps
{
namespace libmass
{

class Averagine
{

  public:
  Averagine(IsotopicDataCstSPtr isotopic_data_csp);
  Averagine(const Averagine &other);

  virtual ~Averagine();

  Averagine &operator=(const Averagine &other);

  void setIsotopicData(IsotopicDataCstSPtr isotopic_data_csp);

  void setContent(QString &symbol, double content);
  double getContent(QString &symbol) const;

  void setFormula(const QString formula_string);

  double getAvgMass() const;

  double computeFormulaAveragineEquivalents(const QString &formula_string = QString());
  double computeFormulaMonoMass(const QString &formula_string = QString());

  bool validate();


  protected:
  IsotopicDataCstSPtr mcsp_isotopicData;
  double m_avg;
  Formula m_formula;
  std::map<QString, double> m_symbolContentMap;

  void setupDefaultConfiguration();
  double computeAvgMass();
};


} // namespace libmass

} // namespace msxps
