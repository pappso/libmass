/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

#include <limits>
#include <vector>
#include <memory>

/////////////////////// Qt includes


/////////////////////// Local includes
#include "globals.hpp"
#include "IsotopicData.hpp"
#include "IsotopicDataBaseHandler.hpp"


namespace msxps
{
namespace libmass
{


// This class specializes IsotopicDataBaseHandler by providing function
// specific for the user's manual configuration of chemical formula associated
// with elements' isotopic data. The format of the data on file is nothing
// like the format used to store Library- or User-Config- isotopic data.
// File format:
//
//[Element]
// symbol C count 100
//[Isotopes] 2
// mass 12.000 prob 0.9899
// mass 13.000 prob 0.010

using SymbolCountMap     = std::map<QString, int>;
using SymbolCountMapIter = SymbolCountMap::iterator;

class IsotopicDataManualConfigHandler : public IsotopicDataBaseHandler
{
  public:
  IsotopicDataManualConfigHandler(const QString &file_name = QString());
  IsotopicDataManualConfigHandler(IsotopicDataSPtr isotopic_data_sp,
                                  const QString &file_name = QString());

  virtual ~IsotopicDataManualConfigHandler();

  virtual void setSymbolCountMap(const SymbolCountMap &map);
  virtual const SymbolCountMap &getSymbolCountMap() const;

  // Load all the isotopic data from the file. file_name cannot be empty and
  // must point to a valid file, of course.
  virtual std::size_t loadData(const QString &file_name) override;

  // Write the isotopic data to a file. Here the is some leeway (file_name might
  // be empty, some alternatives will be searched for).
  virtual std::size_t writeData(const QString &file_name = QString()) override;

  bool newChemicalSet(const QString &symbol,
                      int element_count,
                      const std::vector<IsotopeSPtr> &isotopes,
                      bool update_maps = true);

  QString craftFormula() const;

  protected:
  // Helper data to store symbol/count data.
  SymbolCountMap m_symbolCountMap;

  virtual std::size_t checkConsistency() override;
};

typedef std::shared_ptr<IsotopicDataManualConfigHandler>
  IsotopicDataManualConfigHandlerSPtr;
typedef std::shared_ptr<const IsotopicDataManualConfigHandler>
  IsotopicDataManualConfigHandlerCstSPtr;

} // namespace libmass

} // namespace msxps

