/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once


/////////////////////// Qt includes
#include <QString>
#include <QList>


/////////////////////// Local includes


namespace msxps
{

namespace libmass
{


// #include <libisospec++/isoSpec++.h>
//
// extern const int elem_table_atomicNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double
// elem_table_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double elem_table_mass[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int elem_table_massNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int
// elem_table_extraNeutrons[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const char* elem_table_element[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const char* elem_table_symbol[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const bool elem_table_Radioactive[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double
// elem_table_log_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];

enum class IsotopeFields
{
  ID              = 0,
  ELEMENT         = 1,
  SYMBOL          = 2,
  ATOMIC_NUMBER   = 3,
  MASS            = 4,
  MASS_NUMBER     = 5,
  EXTRA_NEUTRONS  = 6,
  PROBABILITY     = 7,
  LOG_PROBABILITY = 8,
  RADIOACTIVE     = 9,
  LAST            = 10,
};


class Isotope
{

  public:
  Isotope(int id,
          QString element,
          QString symbol,
          int atomicNo,
          double mass,
          int massNo,
          int extraNeutrons,
          double probability,
          double logProbability,
          bool radioactive);

  Isotope(const Isotope &other);
  Isotope(const QString &text);

  virtual ~Isotope();

  bool initialize(const QString &text);

  void setId(int id);
  int getId() const;

  void setElement(const QString &element);
  QString getElement() const;

  void setSymbol(const QString &symbol);
  QString getSymbol() const;

  void setAtomicNo(int atomic_number);
  int getAtomicNo() const;

  void setMass(double mass);
  double getMass() const;

  void setMassNo(int mass_number);
  int getMassNo() const;

  void setExtraNeutrons(int extra_neutrons);
  int getExtraNeutrons() const;

  void setProbability(double probability);
  double getProbability() const;

  void setLogProbability(double log_probability);
  double getLogProbability() const;

  void setRadioactive(bool is_radioactive);
  bool getRadioactive() const;

  int validate(QString *errors_p = nullptr) const;

  virtual Isotope &operator=(const Isotope &other);
  bool operator==(const Isotope &other) const;
  bool operator!=(const Isotope &other) const;

  QString toString() const;

  protected:
  int m_id;
  QString m_element;
  QString m_symbol;
  int m_atomicNo;
  double m_mass;
  int m_massNo;
  int m_extraNeutrons;
  double m_probability;
  double m_logProbability;
  bool m_radioactive;
};

typedef std::shared_ptr<Isotope> IsotopeSPtr;
typedef std::shared_ptr<const Isotope> IsotopeCstSPtr;

} // namespace libmass

} // namespace msxps

