/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#ifndef MONOMER_DICTIONARY_HPP
#define MONOMER_DICTIONARY_HPP


/////////////////////// Qt includes
#include <QString>


/////////////////////// Local includes
#include "PolChemDefEntity.hpp"
#include "Formula.hpp"
#include "Monomer.hpp"


namespace msxps
{

namespace libmass
{


  class MonomerDictionary
  {
    private:
    bool isLineProperSectionDivider(const QString &);
    void skipSection(QTextStream *);
    int parseSection(QTextStream *);

    QHash<QString, QString> m_dictionaryHash;
    bool m_dictionaryLoaded;

    QString m_filePath;

    QStringList m_inputChainStringList;

    int m_inputCodeLength;
    int m_outputCodeLength;

    public:
    MonomerDictionary(QString             = QString(),
                      const QStringList & = QStringList(),
                      int                 = -1,
                      int                 = -1);
    ~MonomerDictionary();

    void setFilePath(QString &);
    void setInputChainStringList(const QStringList &);
    void setInputCodeLength(int);
    void setOutputCodeLength(int);

    bool loadDictionary();

    QStringList *translate(const QStringList & = QStringList());
  };

} // namespace libmass

} // namespace msxps


#endif // MONOMER_DICTIONARY_HPP
