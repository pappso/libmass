/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2024 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Std lib includes
#include <limits>
#include <set>

/////////////////////// Qt includes
#include <QDebug>


/////////////////////// IsoSpec
#include <IsoSpec++/isoSpec++.h>
#include <IsoSpec++/element_tables.h>


// extern const int elem_table_atomicNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double
// elem_table_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double elem_table_mass[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int elem_table_massNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int
// elem_table_extraNeutrons[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const char* elem_table_element[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const char* elem_table_symbol[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const bool elem_table_Radioactive[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double
// elem_table_log_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];


/////////////////////// Local includes
#include <libmass/globals.hpp>
#include <libmass/PeakCentroid.hpp>
#include "IsotopicData.hpp"
#include "IsotopicDataUserConfigHandler.hpp"


namespace msxps
{

namespace libmass
{


IsotopicData::IsotopicData()
{
}


IsotopicData::IsotopicData(const IsotopicData &other)
  : m_isotopes(other.m_isotopes),
    m_symbolMonoMassMap(other.m_symbolMonoMassMap),
    m_symbolAvgMassMap(other.m_symbolAvgMassMap)
{
}


IsotopicData::~IsotopicData()
{
  // qDebug();
}


void
IsotopicData::appendNewIsotope(IsotopeSPtr isotope_sp, bool update_maps)
{
  m_isotopes.push_back(isotope_sp);

  // We have modified the fundamental data, we may need to recompute some data.
  // update_maps might be false when loading data from a file, in which case it
  // is the responsibility of the user to call updateMassMaps() at the end of
  // the file loading.

  if(update_maps)
    updateMassMaps();
}


void
IsotopicData::appendNewIsotopes(const std::vector<IsotopeSPtr> &isotopes,
                                bool update_maps)
{
  std::size_t count_before = m_isotopes.size();

  m_isotopes.insert(m_isotopes.end(), isotopes.begin(), isotopes.end());

  std::size_t count_after = m_isotopes.size();

  if(count_after - count_before != isotopes.size())
    qFatal("Programming error.");

  if(update_maps)
    updateMassMaps();
}


bool
IsotopicData::insertNewIsotope(IsotopeSPtr isotope_sp,
                               std::size_t index,
                               bool update_maps)
{
  // qDebug() << "the size of the data:" << size() << "and" << m_isotopes.size()
  //<< "requested index:" << index;

  // We define that we insert the new isotope before the one at index, as is the
  // convention in the STL and in Qt code.

  if(!m_isotopes.size() || index > m_isotopes.size() - 1)
    {
      appendNewIsotope(isotope_sp, update_maps);
      return true;
    }

  // Convert the index to an iterator.

  std::vector<IsotopeSPtr>::const_iterator iter = m_isotopes.begin() + index;

  // Finally, do the insertion.

  iter = m_isotopes.insert(iter, isotope_sp);

  // qDebug() << "Inserted isotope:" << (*iter)->getSymbol();

  // If inserting an empty isotope in relation to a row insertion in the table
  // view, then update_maps needs to be false because update_maps needs valid
  // symbols for isotopes!

  if(update_maps)
    updateMassMaps();

  // iter points to the inserted isotope.
  return iter != m_isotopes.end();
}

std::vector<IsotopeSPtr>::const_iterator
IsotopicData::eraseIsotopes(std::size_t begin_index,
                            std::size_t end_index,
                            bool update_maps)
{

  // qDebug() << "Erasing isotopes in inclusive index range: [" << begin_index
  //<< "-" << end_index << "] - range is fully inclusive.";

  if(!m_isotopes.size() || begin_index > m_isotopes.size() - 1)
    return m_isotopes.cend();

  std::vector<IsotopeSPtr>::const_iterator iter_begin =
    m_isotopes.cbegin() + begin_index;

  // Sanity check, as this is equivalent to begin_index > m_isotopes.size() -1.
  if(iter_begin == m_isotopes.end())
    return m_isotopes.cend();

  // Let's say that by default, we remove util the last included:
  std::vector<IsotopeSPtr>::const_iterator iter_end = m_isotopes.cend();

  // But, if end_index is less than the last index, then end() has to be the
  // next item after the one at that index.
  if(end_index < m_isotopes.size() - 1)
    iter_end = std::next(m_isotopes.begin() + end_index);

  // At this point we are confident we can assign the proper end() iterator
  // value for the erase function below.

  auto iter = m_isotopes.erase(iter_begin, iter_end);

  if(update_maps)
    {
      // qDebug() << "Now updating masses";
      updateMassMaps();
    }
    // else
    // qDebug() << "Not updating masses";

#if 0
      if(m_isotopes.size())
        {
          qDebug() << "The avg mass of the first isotope symbol in the vector:"
                   << computeAvgMass(
                        getIsotopesBySymbol(m_isotopes.front()->getSymbol()));
        }
#endif

  return iter;
}


bool
IsotopicData::updateMonoMassMap(const QString &symbol)
{

  // For all the common chemical elements found in organic substances, the
  // monoisotopic mass is the mass of the most abundant isotope which
  // happens to be also the lightest isotope. However that is not true for
  // *all* the chemical elements. We thus need to iterate in the isotopes
  // of each symbol in the vector of isotopes and record the mass of the
  // isotope that is most abundant.

  // We do only work with a single symbol here.

  if(symbol.isEmpty())
    qFatal("Programming error. The symbol cannot be empty.");

  double greatest_abundance = std::numeric_limits<double>::min();
  double mass               = 0.0;

  IsotopeCstIteratorPair iter_pair = getIsotopesBySymbol(symbol);

  while(iter_pair.first != iter_pair.second)
    {
      if((*iter_pair.first)->getProbability() > greatest_abundance)
        {
          greatest_abundance = (*iter_pair.first)->getProbability();
          mass               = (*iter_pair.first)->getMass();
        }

      ++iter_pair.first;
    }

  // At this point we have the mono mass of the currently iterated symbol.

  std::pair<std::map<QString, double>::const_iterator, bool> res_pair =
    m_symbolMonoMassMap.insert_or_assign(symbol, mass);

  // return true if was inserted (that is, the symbol was not there) or
  // false if the value was assigned to an existing key.

  return res_pair.second;
}


std::size_t
IsotopicData::updateMonoMassMap()
{

  // For all the common chemical elements found in organic substances, the
  // monoisotopic mass is the mass of the most abundant isotope which
  // happens to be also the lightest isotope. However that is not true for
  // *all* the chemical elements. We thus need to iterate in the isotopes
  // of each symbol in the vector of isotopes and record the mass of the
  // isotope that is most abundant.

  m_symbolMonoMassMap.clear();

  // Get the list of all the isotope symbols.

  std::size_t count = 0;

  std::vector<QString> all_symbols = getUniqueSymbolsInOriginalOrder();

  for(auto symbol : all_symbols)
    {
      updateMonoMassMap(symbol);
      ++count;
    }

  return count;
}


bool
IsotopicData::updateAvgMassMap(const QString &symbol)
{
  // For each chemical element (that is either name or symbol), we need to
  // compute the sum of the probabilities of all the corresponding
  // isotopes. Once that sum (which should be 1) is computed, it is
  // possible to compute the averag mass of "that symbol", so to say.

  // We do only work with a single symbol here.

  if(symbol.isEmpty())
    qFatal("Programming error. The symbol cannot be empty.");

#if 0

  // Now this is in a function per se:
  // computeAvgMass(IsotopeCstIteratorPair iter_pair, bool *ok)

  double cumulated_probabilities = 0.0;
  double avg_mass                = 0.0;

  IsotopeCstIteratorPair pair = getIsotopesBySymbol(symbol);

  // We need to use that iterator twice, so we do make a copy.

  IsotopeCstIterator local_iter = pair.first;

  while(local_iter != pair.second)
    {
      cumulated_probabilities += (*pair.first)->getProbability();

      ++local_iter;
    }

  // Sanity check
  if(!cumulated_probabilities)
    qFatal("Programming error. The cumulated probabilities cannot be naught.");

  // And at this point we can compute the average mass.

  local_iter = pair.first;

  while(local_iter != pair.second)
    {
      avg_mass += (*local_iter)->getMass() *
                  ((*local_iter)->getProbability() / cumulated_probabilities);

      ++local_iter;
    }
#endif

  IsotopeCstIteratorPair iter_pair = getIsotopesBySymbol(symbol);

  // qDebug() << "For symbol" << symbol << "the iter range was found to
  // be of distance:"
  //<< std::distance(iter_pair.first, iter_pair.second)
  //<< "with symbol: " << (*iter_pair.first)->getSymbol();

  std::vector<QString> errors;

  double avg_mass = computeAvgMass(iter_pair, errors);

  if(errors.size())
    {
      qFatal(
        "The calculation of the average mass for a given "
        "symbol failed.");
    }

  std::pair<std::map<QString, double>::const_iterator, bool> res_pair =
    m_symbolAvgMassMap.insert_or_assign(symbol, avg_mass);

  // return true if was inserted (that is, the symbol was not there) or
  // false if the value was assigned to an existing key.

  return res_pair.second;
}


std::size_t
IsotopicData::updateAvgMassMap()
{
  // For each chemical element (that is either name or symbol), we need to
  // compute the sum of the probabilities of all the corresponding
  // isotopes. Once that sum (which should be 1) is computed, it is
  // possible to compute the averag mass of "that symbol", so to say.

  m_symbolAvgMassMap.clear();

  // Get the list of all the isotope symbols.

  std::size_t count = 0;

  std::vector<QString> all_symbols = getUniqueSymbolsInOriginalOrder();

  for(auto symbol : all_symbols)
    {
      updateAvgMassMap(symbol);
      ++count;
    }

  return count;
}


double
IsotopicData::computeAvgMass(IsotopeCstIteratorPair iter_pair,
                             std::vector<QString> &errors) const
{
  // We get an iterator range for which we need to compute the average
  // mass. No check whatsoever, we do what we are asked to do. This
  // function is used to check or document user's actions in some places.

  double avg_mass = 0.0;

  // qDebug() << "Computing avg mass for iter range of distance:"
  //<< std::distance(iter_pair.first, iter_pair.second)
  //<< "with symbol: " << (*iter_pair.first)->getSymbol();

  if(iter_pair.first == m_isotopes.cend())
    {
      qDebug() << "First iterator is actually end of m_isotopes.";
      errors.push_back(
        QString("First iterator is actually end of m_isotopes."));

      return avg_mass;
    }

  std::size_t previous_error_count = errors.size();

  double cumulated_probabilities = getCumulatedProbabilities(iter_pair, errors);

  if(errors.size() > previous_error_count || !cumulated_probabilities)
    {
      // There was an error. We want to report it.

      errors.push_back(
        QString("Failed to compute the cumulated probabilities needed to "
                "compute the average mass."));

      return avg_mass;
    }

  // At this point, compute the average mass.

  while(iter_pair.first != iter_pair.second)
    {
      avg_mass +=
        (*iter_pair.first)->getMass() *
        ((*iter_pair.first)->getProbability() / cumulated_probabilities);

      // qDebug() << "avg_mass:" << avg_mass;

      ++iter_pair.first;
    }

  return avg_mass;
}


void
IsotopicData::updateMassMaps(const QString &symbol)
{
  updateMonoMassMap(symbol);
  updateAvgMassMap(symbol);
}


std::size_t
IsotopicData::updateMassMaps()
{
  std::size_t count = updateMonoMassMap();

  if(!count)
    qDebug("There are no isotopes. Cleared the mono mass map.");

  count = updateAvgMassMap();

  if(!count)
    qDebug("There are no isotopes. Cleared the avg mass map.");

  // Number of symbols for which the mass was updated.
  return count;
}


double
IsotopicData::getMonoMassBySymbol(const QString &symbol, bool *ok) const
{
  if(symbol.isEmpty())
    qFatal("Programming error. The symbol cannot be empty.");

  // qDebug() << "The symbol/mono mass map has size:"
  //<< m_symbolMonoMassMap.size();

  std::map<QString, double>::const_iterator found_iter =
    m_symbolMonoMassMap.find(symbol);

  if(found_iter == m_symbolMonoMassMap.cend())
    {
      qDebug() << "Failed to find the symbol in the map.";

      if(ok != nullptr)
        {
          *ok = false;
          return 0.0;
        }
    }

  if(ok)
    *ok = true;

  // qDebug() << "The mono mass is found to be" << found_iter->second;

  return found_iter->second;
}


double
IsotopicData::getMonoMass(IsotopeCstIteratorPair iter_pair,
                          std::vector<QString> &errors) const
{
  // The mono mass of a set of isotopes is the mass of the most abundant
  // isotope (not the lightest!).

  double mass               = 0.0;
  double greatest_abundance = 0.0;

  if(iter_pair.first == m_isotopes.cend())
    {
      qDebug() << "First iterator is actually end of m_isotopes.";

      errors.push_back(
        QString("First iterator is actually end of m_isotopes."));

      return mass;
    }

  while(iter_pair.first != iter_pair.second)
    {
      if((*iter_pair.first)->getProbability() > greatest_abundance)
        {
          greatest_abundance = (*iter_pair.first)->getProbability();
          mass               = (*iter_pair.first)->getMass();
        }

      ++iter_pair.first;
    }

  // qDebug() << "The mono mass is found to be" << mass;

  return mass;
}


double
IsotopicData::getAvgMassBySymbol(const QString &symbol, bool *ok) const
{
  if(symbol.isEmpty())
    qFatal("Programming error. The symbol cannot be empty.");

  // qDebug() << "The symbol/avg mass map has size:" <<
  // m_symbolAvgMassMap.size();

  std::map<QString, double>::const_iterator found_iter =
    m_symbolAvgMassMap.find(symbol);

  if(found_iter == m_symbolAvgMassMap.cend())
    {
      qDebug() << "Failed to find the symbol in the map.";

      if(ok != nullptr)
        {
          *ok = false;
          return 0.0;
        }
    }

  if(ok)
    *ok = true;

  // qDebug() << "The avg mass is found to be" << found_iter->second;

  return found_iter->second;
}


double
IsotopicData::getCumulatedProbabilitiesBySymbol(
  const QString &symbol, std::vector<QString> &errors) const
{
  qDebug() << "symbol: " << symbol;

  double cumulated_probabilities = 0.0;

  // We'll need this to calculate the indices of the isotopes in the
  // m_isotopes vector.
  IsotopeVectorCstIterator iter_begin = m_isotopes.cbegin();

  // Get the isotopes iter range for symbol.
  IsotopeCstIteratorPair iter_pair = getIsotopesBySymbol(symbol);

  while(iter_pair.first != iter_pair.second)
    {
      QString error_text = "";

      int error_count = (*iter_pair.first)->validate(&error_text);

      if(error_count)
        {
          error_text.prepend(
            QString("Isotope symbol %1 at index %2: ")
              .arg(symbol)
              .arg(std::distance(iter_begin, iter_pair.first)));

          errors.push_back(error_text);
          cumulated_probabilities = 0.0;
        }
      else
        cumulated_probabilities += (*iter_pair.first)->getProbability();

      ++iter_pair.first;
    }

  return cumulated_probabilities;
}


double
IsotopicData::getCumulatedProbabilities(IsotopeCstIteratorPair iter_pair,
                                        std::vector<QString> &errors) const
{
  double cumulated_probabilities = 0.0;

  if(iter_pair.first == m_isotopes.cend())
    {
      qDebug() << "First iterator is actually end of m_isotopes.";

      errors.push_back(
        QString("First iterator is actually end of m_isotopes."));

      return cumulated_probabilities;
    }

  while(iter_pair.first != iter_pair.second)
    {
      cumulated_probabilities += (*iter_pair.first)->getProbability();

      ++iter_pair.first;
    }

  // qDebug() << "cumulated_probabilities:" << cumulated_probabilities;

  return cumulated_probabilities;
}


IsotopeCstIteratorPair
IsotopicData::getIsotopesBySymbol(const QString &symbol) const
{

  if(symbol.isEmpty())
    return IsotopeCstIteratorPair(m_isotopes.cend(), m_isotopes.cend());

  // We want to "extract" from the vector of isotopes, the ones that share
  // the same symbol under the form of a [begin,end[ pair of iterators.

  //////////////////////////// ASSUMPTION /////////////////////////
  //////////////////////////// ASSUMPTION /////////////////////////
  // The order of the isotopes is not alphabetical (it is the order of the
  // atomic number. This function works on the assumption that all the
  // isotopes of a given symbol are clustered together in the isotopes
  // vector with *no* gap in between.
  //////////////////////////// ASSUMPTION /////////////////////////
  //////////////////////////// ASSUMPTION /////////////////////////

  // We will start iterating in the vector of isotopes at the very
  // beginning.
  std::vector<IsotopeSPtr>::const_iterator iter = m_isotopes.cbegin();

  // Never reach the end of the isotoes vector.
  std::vector<IsotopeSPtr>::const_iterator iter_end = m_isotopes.cend();

  // This iterator will be the end iterator of the range that comprises
  // the isotopes all sharing the same symbol. We initialize it to
  // iter_end in case we do not find the symbol at all. Otherwise it will
  // be set to the right value.
  std::vector<IsotopeSPtr>::const_iterator symbol_iter_end = iter_end;

  while(iter != iter_end)
    {
      QString current_symbol = (*iter)->getSymbol();

      if(current_symbol != symbol)
        {
          // qDebug() << "Current isotope has symbol" << current_symbol
          //<< "and we are looking for" << symbol
          //<< "incrementing to next position.";
          ++iter;
        }
      else
        {

          // qDebug() << "Current isotope has symbol" << current_symbol
          //<< "and we are looking for" << symbol
          //<< "with mass:" << (*iter)->getMass()
          //<< "Now starting inner iteration loop.";

          // At this point we encountered one isotope that has the right
          // symbol. The iterator "iter" will not change anymore because
          // of the inner loop below that will go on iterating in vector
          // using another set of iterators. "iter" will thus point
          // correctly to the first isotope in the vector having the right
          // symbol.

          // Now in this inner loop, continue iterating in the vector,
          // starting at the present position and go on as long as the
          // encountered isotopes have the same symbol.

          // Set then end iterator to the current position and increment
          // to the next one, since current position has been iterated
          // into already (position is stored in "iter") and go on. This
          // way, if there was a single isotope by given symbol,
          // "symbol_iter_end" rightly positions at the next isotope. If
          // that is not the case, its value updates and is automatically
          // set to the first isotope that has not the right symbol (or
          // will be set to iter_end if that was the last set of isotopes
          // in the vector).

          symbol_iter_end = iter;
          ++symbol_iter_end;

          while(symbol_iter_end != iter_end)
            {
              // qDebug() << "Inner loop iteration in: "
              //<< (*symbol_iter_end)->getSymbol()
              //<< "while we search for" << symbol
              //<< "Iterated isotope has mass:"
              //<< (*symbol_iter_end)->getMass();

              if((*symbol_iter_end)->getSymbol() == symbol)
                {
                  // We can iterate further in the isotopes vector because
                  // the current iterator pointed to an isotope that still
                  // had the right symbol. qDebug()
                  //<< "Good symbol, going to next inner iteration
                  // position.";
                  ++symbol_iter_end;
                }
              else
                {
                  // We currently iterate in an isotope that has a symbol
                  // different from the searched one: the symbol_iter_end
                  // thus effectively plays the role of the
                  // iterator::end() of the isotopes range having the
                  // proper symbol. qDebug()
                  //<< "The symbols do not match, breaking the inner
                  // loop.";
                  break;
                }
            }

          // We can break the outer loop because we have necessarily gone
          // through the isotopes of the requested symbol at this point.
          // See at the top of this outer loop that when an isotope has
          // not the right symbol, the iter is incremented
          break;
        }
      // End of block "the iterated symbol is the searched one"
    }
  // End of outer while loop

  // qDebug() << "For symbol" << symbol << "the iter range was found to be:"
  //<< std::distance(iter, symbol_iter_end);

  return IsotopeCstIteratorPair(iter, symbol_iter_end);
}


std::size_t
IsotopicData::getIsotopeCountBySymbol(const QString &symbol) const
{
  IsotopeCstIteratorPair iter_pair = getIsotopesBySymbol(symbol);

  return std::distance(iter_pair.first, iter_pair.second);
}


IsotopeCstIteratorPair
IsotopicData::getIsotopesByName(const QString &name) const
{
  if(name.isEmpty())
    return IsotopeCstIteratorPair(m_isotopes.cend(), m_isotopes.cend());

  // We want to "extract" from the vector of isotopes, the ones that share
  // the same name under the form of a [begin,end[ pair of iterators.

  //////////////////////////// ASSUMPTION /////////////////////////
  //////////////////////////// ASSUMPTION /////////////////////////
  // The order of the isotopes is not alphabetical (it is the order of the
  // atomic number. This function works on the assumption that all the
  // isotopes of a given symbol are clustered together in the isotopes
  // vector with *no* gap in between.
  //////////////////////////// ASSUMPTION /////////////////////////
  //////////////////////////// ASSUMPTION /////////////////////////

  // We will start iterating in the vector of isotopes at the very
  // beginning.
  std::vector<IsotopeSPtr>::const_iterator iter = m_isotopes.cbegin();

  // Never reach the end of the isotoes vector.
  std::vector<IsotopeSPtr>::const_iterator iter_end = m_isotopes.cend();

  // This iterator will be the end iterator of the range that comprises
  // the isotopes all sharing the same name. We initialize it to iter_end
  // in case we do not find the name at all. Otherwise it will be set to
  // the right value.
  std::vector<IsotopeSPtr>::const_iterator name_iter_end = iter_end;

  while(iter != iter_end)
    {
      QString current_name = (*iter)->getElement();

      if(current_name != name)
        ++iter;

      // At this point we encountered one isotope that has the right name.
      // The iterator "iter" will not change anymore because of the inner
      // loop below that will go on iterating in vector using another set
      // of iterators. "iter" will thus point correctly to the first
      // isotope in the vector having the right name.

      // Now in this inner loop, continue iterating in the vector,
      // starting at the present position and go on as long as the
      // encountered isotopes have the same name.

      // Set then end iterator to the current position and increment to
      // the next one, since current position has been iterated into
      // already (position is stored in "iter") and go on. This way, if
      // there was a single isotope by given name, "name_iter_end" rightly
      // positions at the next isotope. If that is not the case, its value
      // updates and is automatically set to the first isotope that has
      // not the right name (or will be set to iter_end if that was the
      // last set of isotopes in the vector).

      name_iter_end = iter;
      ++name_iter_end;

      while(name_iter_end != iter_end)
        {
          // qDebug() << "Iterating in: " << (*name_iter_end)->getElement()
          //<< "with mass:" << (*name_iter_end)->getMass();

          if((*name_iter_end)->getElement() == name)
            {
              // We can iterate further in the isotopes vector because the
              // current iterator pointed to an isotope that still had the
              // right name.
              // qDebug() << "Going to next iterator position.";
              ++name_iter_end;
            }
          else
            {
              // We currently iterate in an isotope that has a name
              // different from the searched one: the name_iter_end thus
              // effectively plays the role of the iterator::end() of the
              // isotopes range having the proper name.
              break;
            }
        }
    }

  // qDebug() << "The iter range was found to be:"
  //<< std::distance(iter, name_iter_end);

  return IsotopeCstIteratorPair(iter, name_iter_end);
}


std::vector<QString>
IsotopicData::getUniqueSymbolsInOriginalOrder() const
{
  // The way IsoSpec works and the way we configure the
  // symbol/count/isotopes data depend in some situations on the order in
  // which the Isotopes were read either from the library tables or from
  // user-created disk files.
  //
  // This function wants to craft a list of unique isotope symbols exactly
  // as they are found in the member vector.

  std::set<QString> symbol_set;

  std::vector<QString> symbols;

  for(auto isotope_sp : m_isotopes)
    {
      QString symbol = isotope_sp->getSymbol();

      auto res = symbol_set.insert(symbol);
      // res.second is true if the symbol was inserted, which mean it did
      // not exist in the set.
      if(res.second)
        symbols.push_back(symbol);
    }

  return symbols;
}


bool
IsotopicData::containsSymbol(const QString &symbol, int *count) const
{
  IsotopeCstIteratorPair iter_pair = getIsotopesBySymbol(symbol);

  if(iter_pair.first == m_isotopes.cend())
    return false;

  // Now compute the distance between the two iterators to know how many
  // isotopes share the same chemical element symbol.

  if(count != nullptr)
    *count = std::distance(iter_pair.first, iter_pair.second);

  return true;
}


bool
IsotopicData::containsName(const QString &name, int *count) const
{
  IsotopeCstIteratorPair iter_pair = getIsotopesByName(name);

  if(iter_pair.first == m_isotopes.cend())
    return false;

  // Now compute the distance between the two iterators to know how many
  // isotopes share the same chemical element symbol.

  if(count != nullptr)
    *count = std::distance(iter_pair.first, iter_pair.second);

  return true;
}


QString
IsotopicData::isotopesAsStringBySymbol(const QString &symbol) const
{
  if(symbol.isEmpty())
    qFatal("Programming error. The symbol cannot be empty.");

  QString text;

  IsotopeCstIteratorPair iter_pair = getIsotopesBySymbol(symbol);

  if(iter_pair.first == m_isotopes.cend())
    {
      qDebug() << "The symbol was not found in the vector of isotopes.";
      return text;
    }

  while(iter_pair.first != iter_pair.second)
    {
      text += (*iter_pair.first)->toString();
      text += "\n";
    }

  return text;
}


bool
IsotopicData::isMonoMassIsotope(IsotopeCstSPtr isotope_csp)
{
  // Is the passed isotope the one that has the greatest abundance among
  // all the isotopes having the same symbol.

  if(isotope_csp == nullptr)
    qFatal("Programming error. The isotope pointer cannot be nullptr.");

  QString symbol(isotope_csp->getSymbol());

  double mass = m_symbolMonoMassMap.at(symbol);

  if(mass == isotope_csp->getMass())
    return true;

  return false;
}


const std::vector<IsotopeSPtr> &
IsotopicData::getIsotopes() const
{
  return m_isotopes;
}


IsotopicData &
IsotopicData::operator=(const IsotopicData &other)
{
  if(&other == this)
    return *this;

  m_isotopes          = other.m_isotopes;
  m_symbolMonoMassMap = other.m_symbolMonoMassMap;
  m_symbolAvgMassMap  = other.m_symbolAvgMassMap;

  return *this;
}


std::size_t
IsotopicData::size() const
{
  return m_isotopes.size();
}

std::size_t
IsotopicData::getUniqueSymbolsCount() const
{
  // Go trough the vector of IsotopeSPtr and check how many different
  // symbols it contains.

  std::set<QString> symbols_set;

  std::vector<IsotopeSPtr>::const_iterator iter     = m_isotopes.cbegin();
  std::vector<IsotopeSPtr>::const_iterator iter_end = m_isotopes.cend();

  while(iter != iter_end)
    {
      symbols_set.insert((*iter)->getSymbol());
      ++iter;
    }

  return symbols_set.size();
}


int
IsotopicData::validate(std::vector<QString> &errors) const
{
  int total_error_count = 0;

  IsotopeVectorCstIterator iter_begin = m_isotopes.cbegin();
  IsotopeVectorCstIterator iter_end   = m_isotopes.cend();

  IsotopeVectorCstIterator iter = iter_begin;

  while(iter != iter_end)
    {
      QString error_text = "";

      int error_count = (*iter)->validate(&error_text);

      if(error_count)
        {
          error_text.prepend(QString("Isotope at index %1: ")
                               .arg(std::distance(iter_begin, iter)));

          errors.push_back(error_text);

          total_error_count += error_count;
        }

      ++iter;
    }

  return total_error_count;
}


bool
IsotopicData::validateBySymbol(const QString &symbol,
                               std::vector<QString> &errors) const
{
  // This function is more powerful than the other one because it actually
  // looks the integrity of the data symbol-wise. For example, a set of
  // isotopes by the same symbol cannot have a cumulated probability greater
  // than 1. If that were the case, that would be reported.

  // qDebug() << "symbol: " << symbol;

  // Validating by symbol means looking into each isotope that has the same
  // 'symbol' and validating each one separately. However, it also means looking
  // if all the cumulated isotope probabilities (abundances) for a given symbol
  // are different than 1.

  // Record the size of the errors vector so that we can report if we added
  // errors in this block.
  std::size_t previous_error_count = errors.size();

  double cumulated_probabilities =
    getCumulatedProbabilitiesBySymbol(symbol, errors);

  if(cumulated_probabilities != 1)
    {
      QString prob_error =
        QString(
          "Isotope symbol %1: has cumulated probabilities not equal to 1.\n")
          .arg(symbol);

      errors.push_back(prob_error);
    }

  return errors.size() > previous_error_count;
}


bool
IsotopicData::validateAllBySymbol(std::vector<QString> &errors) const
{
  // This validation of all the isotopes as grouped by symbol is more
  // informative than the validation isotope by isotope idependently
  // because it can spot cumulated probabilities problems.

  std::size_t previous_error_count = errors.size();

  std::vector<QString> all_symbols = getUniqueSymbolsInOriginalOrder();

  for(auto symbol : all_symbols)
    {
      QString error_text = "";

      validateBySymbol(symbol, errors);
    }

  return errors.size() > previous_error_count;
}


void
IsotopicData::replace(IsotopeSPtr old_isotope_sp, IsotopeSPtr new_isotope_sp)
{
  std::replace(
    m_isotopes.begin(), m_isotopes.end(), old_isotope_sp, new_isotope_sp);
}


void
IsotopicData::clear()
{
  m_isotopes.clear();
  m_symbolMonoMassMap.clear();
  m_symbolAvgMassMap.clear();
}


IsotopicDataSPtr
loadIsotopicDataFromFile(const QString &file_name, QString &error)
{
  qDebug();

  IsotopicDataSPtr isotopic_data_sp = std::make_shared<IsotopicData>();

  if(file_name.isEmpty())
    {
      error = "Failed to set the file name.";
      return nullptr;
    }

  QFile file(file_name);

  if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
      error = "Failed to open the file for reading.";
      return nullptr;
    }

  IsotopicDataUserConfigHandler isotopic_data_handler(isotopic_data_sp);

  if(!isotopic_data_handler.loadData(file_name))
    {
      error = "Failed to load the isotopic data.";
      file.close();

      return nullptr;
    }

  qDebug() << "Loaded isotopic data from file" << file_name;

  file.close();

  return isotopic_data_sp;
}


} // namespace libmass

} // namespace msxps


#if 0

Example from IsoSpec.

const int elementNumber = 2;
const int isotopeNumbers[2] = {2,3};

const int atomCounts[2] = {2,1};


const double hydrogen_masses[2] = {1.00782503207, 2.0141017778};
const double oxygen_masses[3] = {15.99491461956, 16.99913170, 17.9991610};

const double* isotope_masses[2] = {hydrogen_masses, oxygen_masses};

const double hydrogen_probs[2] = {0.5, 0.5};
const double oxygen_probs[3] = {0.5, 0.3, 0.2};

const double* probs[2] = {hydrogen_probs, oxygen_probs};

IsoLayeredGenerator iso(Iso(elementNumber, isotopeNumbers, atomCounts, isotope_masses, probs), 0.99);

#endif
