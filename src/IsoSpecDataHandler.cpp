/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright (C) 2009--2020 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// IsoSpec
#include <IsoSpec++/isoSpec++.h>
#include <IsoSpec++/element_tables.h>


// extern const int elem_table_atomicNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double
// elem_table_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double elem_table_mass[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int elem_table_massNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const int
// elem_table_extraNeutrons[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const char* elem_table_element[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const char* elem_table_symbol[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const bool elem_table_Radioactive[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
// extern const double
// elem_table_log_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];


/////////////////////// Local includes
#include <libmass/globals.hpp>
#include <libmass/PeakCentroid.hpp>
#include "IsoSpecDataHandler.hpp"


namespace msxps
{

namespace libmass
{


IsoSpecDataHandler::IsoSpecDataHandler()
{
}


IsoSpecDataHandler::~IsoSpecDataHandler()
{
  // qDebug();
}


std::size_t
IsoSpecDataHandler::checkConsistencyIsoSpecTables()
{
  std::size_t arrayLength = sizeof(IsoSpec::elem_table_atomicNo) /
                            sizeof(IsoSpec::elem_table_atomicNo[0]);

  // qDebug() << "The array length is:" << arrayLength;

  std::size_t currentLength = sizeof(IsoSpec::elem_table_probability) /
                              sizeof(IsoSpec::elem_table_probability[0]);
  if(currentLength != arrayLength)
    {
      qDebug()
        << "Found corruption: at least two arrays are not of the same length."
        << "currentLength:" << currentLength;

      return 0;
    }

  currentLength =
    sizeof(IsoSpec::elem_table_mass) / sizeof(IsoSpec::elem_table_mass[0]);
  if(currentLength != arrayLength)
    {
      qDebug()
        << "Found corruption: at least two arrays are not of the same length."
        << "currentLength:" << currentLength;

      return 0;
    }

  currentLength =
    sizeof(IsoSpec::elem_table_massNo) / sizeof(IsoSpec::elem_table_massNo[0]);
  if(currentLength != arrayLength)
    {
      qDebug()
        << "Found corruption: at least two arrays are not of the same length."
        << "currentLength:" << currentLength;

      return 0;
    }

  currentLength = sizeof(IsoSpec::elem_table_extraNeutrons) /
                  sizeof(IsoSpec::elem_table_extraNeutrons[0]);
  if(currentLength != arrayLength)
    {
      qDebug()
        << "Found corruption: at least two arrays are not of the same length."
        << "currentLength:" << currentLength;

      return 0;
    }

  currentLength = sizeof(IsoSpec::elem_table_element) /
                  sizeof(IsoSpec::elem_table_element[0]);
  if(currentLength != arrayLength)
    {
      qDebug()
        << "Found corruption: at least two arrays are not of the same length."
        << "currentLength:" << currentLength;

      return 0;
    }

  currentLength =
    sizeof(IsoSpec::elem_table_symbol) / sizeof(IsoSpec::elem_table_symbol[0]);
  if(currentLength != arrayLength)
    {
      qDebug()
        << "Found corruption: at least two arrays are not of the same length."
        << "currentLength:" << currentLength;

      return 0;
    }

  currentLength = sizeof(IsoSpec::elem_table_Radioactive) /
                  sizeof(IsoSpec::elem_table_Radioactive[0]);
  if(currentLength != arrayLength)
    {
      qDebug()
        << "Found corruption: at least two arrays are not of the same length."
        << "currentLength:" << currentLength;

      return 0;
    }

  currentLength = sizeof(IsoSpec::elem_table_log_probability) /
                  sizeof(IsoSpec::elem_table_log_probability[0]);
  if(currentLength != arrayLength)
    {
      qDebug()
        << "Found corruption: at least two arrays are not of the same length."
        << "currentLength:" << currentLength;

      return 0;
    }

  return currentLength;
}


int
IsoSpecDataHandler::loadIsoSpecLibraryTables()
{
  // qDebug();

  // We need to allocate one Isotope instance for each element
  // in the various arrays in the IsoSpec++ source code header file.

  // extern const int elem_table_ID[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const int elem_table_atomicNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const double
  // elem_table_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const double elem_table_mass[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const int elem_table_massNo[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const int
  // elem_table_extraNeutrons[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];
  // extern const char*
  // elem_table_element[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES]; extern const
  // char* elem_table_symbol[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES]; extern const
  // bool elem_table_Radioactive[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES]; extern
  // const double
  // elem_table_log_probability[ISOSPEC_NUMBER_OF_ISOTOPIC_ENTRIES];

  // Big sanity check, all the arrays must be the same length!
  std::size_t arrayLength = checkConsistencyIsoSpecTables();
  if(arrayLength < 1)
    return false;

  // At this point we know everything is fine. just iterate in the array.
  // Another last sanity check:
  if(IsoSpec::isospec_number_of_isotopic_entries != arrayLength)
    {
      qFatal("Found corruption: the size of arrays is not like expected.");
    }

  // Clear the vector first, in case we use this function multiple times.
  m_isotopes.clear();

  for(std::size_t iter = 0; iter < arrayLength; ++iter)
    {

      QString elem_element = QString(IsoSpec::elem_table_element[iter]);

      // These are the last items in the various tables. We do not handle them
      // at the moment.
      if(elem_element == "electron" || elem_element == "missing electron" ||
         elem_element == "protonation")
        continue;

      IsotopeCstSPtr isotope_csp = std::make_shared<const Isotope>(
        IsoSpec::elem_table_ID[iter],
        QString(IsoSpec::elem_table_element[iter]),
        QString(IsoSpec::elem_table_symbol[iter]),
        IsoSpec::elem_table_atomicNo[iter],
        IsoSpec::elem_table_mass[iter],
        IsoSpec::elem_table_massNo[iter],
        IsoSpec::elem_table_extraNeutrons[iter],
        IsoSpec::elem_table_probability[iter],
        IsoSpec::elem_table_log_probability[iter],
        IsoSpec::elem_table_Radioactive[iter]);

      m_isotopes.push_back(isotope_csp);
    }

  return m_isotopes.size();
}


bool
IsoSpecDataHandler::writeIsotopeDataToFile(const QString &file_name)
{
  // We need to iterate into the various widgets, exactly as done for the
  // run of the manual configuration.

  // First run the validation which has also the side effect to set aside in
  // member variables the needed atomistic data to later run IsoSpec or
  // create the config string.

  std::size_t elementNumber = validateManualConfig();

  if(!elementNumber)
    {
      // The validation function creates all the necessary feedback.
      return false;
    }

  QString configuration;

  for(std::vector<std::pair<QString, int>>::iterator it =
        m_atomCountPairVector.begin();
      it != m_atomCountPairVector.end();
      ++it)
    {
      std::pair<QString, int> atomSymbolCount = *it;

      QString symbol = it->first;
      int atomCount  = it->second;

      configuration += QString(
                         "[atom]\n"
                         "\tsymbol %1 count %2\n")
                         .arg(symbol)
                         .arg(atomCount);

      // Get the matching Atom from the user-configured atom list.

      bool atomFound = false;

      for(std::vector<Atom>::iterator jt = m_userManualAtomVector.begin();
          jt != m_userManualAtomVector.end();
          ++jt)
        {
          Atom atom = *jt;

          if(atom.symbol() == symbol)
            {
              int isotopeCount = atom.isotopeList().size();

              configuration += QString("\t[isotopes] %1\n").arg(isotopeCount);

              for(int jter = 0; jter < isotopeCount; ++jter)
                {
                  Isotope *isotope = atom.isotopeList().at(jter);

                  configuration += QString("\t\tmass %1 prob %2\n")
                                     .arg(isotope->mass(), 0, 'f', 60)
                                     .arg(isotope->abundance(), 0, 'f', 60);
                }

              atomFound = true;
            }
          else
            continue;
        }
      // Finished searching for atom symbol in the list of user manual
      // configuration atom vector.

      if(!atomFound)
        qFatal("Programming error.");

      configuration += "\n";
    }
  // Finished iterating in the vector of all the symbols found in the
  // formula submitted by the user.

  // qDebug() << "Configuration:" << configuration;

  // At this point let the user choose a file for that config.

  QFileDialog fileDlg(this);
  fileDlg.setFileMode(QFileDialog::AnyFile);
  fileDlg.setAcceptMode(QFileDialog::AcceptSave);

  QString fileName = QFileDialog::getSaveFileName(
    this, tr("Save configuration"), QDir::home().absolutePath());

  if(!fileName.isEmpty())
    {

      QFile file(fileName);
      if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
          message("Failed to open file for writing.", 5000);
          return false;
        }

      QTextStream out(&file);

      out << configuration;

      out.flush();

      file.close();
    }

  message(QString("Configuration saved in %1").arg(fileName), 5000);

  return true;
}


}

bool
IsoSpecDataHandler::loadIsoSpecStandardUserTables()
{
  // qDebug();

  // The format of the file from which data is to be loaded is mimicking the
  // tables that are defined as a number of C arrays in the libIsoSpec++
  // header element_tables.c/h files.

  // When the dialog window is opened, these C arrays are parsed in the
  // IsoSpecDataHandler::initializeIsoSpecStandardStaticEntityList() function. We
  // need to mimick this function but reading data from text files. The text
  // files were initially created by the user with the
  // saveIsoSpecStandardStaticTables() function. These files are TSV
  // (tab-separated values).

  // This function is not like initializeIsoSpecStandardStaticEntityList,
  // because here we might already have some items in the model/table view.
  // Thus we first append all the parsed Isotope instances to a
  // local list, then we make sure we remove all the items from the model and
  // we append the new ones there. The model has a pointer to *this dialog
  // entity list.

  // Because we need to ensure that *this m_isoSpecStandardUserEntityList, the
  // model and the view are consistent, we delegate all removal/additions to
  // the model.

  QString fileName = QFileDialog::getOpenFileName(
    this, tr("Load IsoSpec table"), QDir::home().absolutePath());

  if(fileName.isEmpty())
    {
      message("The file name to read from is empty!", 5000);
      return false;
    }

  QFile file(fileName);
  if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
      message("Failed to open file for reading.", 5000);
      return false;
    }

  QRegularExpression commentRegexp("^\\s+#");

  // Local lists and vector for storing the parsed data until the end of the
  // file. When everything was fine, then, move the data to the member
  // lists/vecotr.
  QList<Isotope *> isoSpecEntityList;
  QList<Atom *> isoSpecAtomList;

  Atom *atom = nullptr;

  QString lastElement;

  // Now start reading from file

  int iter = 0;

  QTextStream in(&file);
  while(!in.atEnd())
    {
      QString line = in.readLine();

      // qDebug() << "line:" << line;

      // Ignore empty or comment lines
      if(line.length() < 1)
        continue;

      QRegularExpressionMatch match = commentRegexp.match(line);
      if(match.hasMatch())
        continue;

      QStringList dataStringList = line.split("\t", Qt::SkipEmptyParts);

      if(dataStringList.size() <
         static_cast<int>(IsoSpecTableViewColumns::TOTAL_COLUMNS))
        {
          message("Failed to load data. Make sure the file is not corrupted.",
                  5000);
          return false;
        }

      // This is the order in which the data must be set in the file for each
      // line.

      QString element, symbol;
      int id, atomicNo, massNo, extraNeutrons;
      double mass, probability, logProbability;
      bool radioactive;

      bool ok    = false;
      int errors = 0;

      id = dataStringList.at(static_cast<int>(IsoSpecTableViewColumns::ID))
             .toInt(&ok);
      if(!ok)
        ++errors;

      element =
        dataStringList.at(static_cast<int>(IsoSpecTableViewColumns::ELEMENT));

      symbol =
        dataStringList.at(static_cast<int>(IsoSpecTableViewColumns::SYMBOL));

      atomicNo =
        dataStringList.at(static_cast<int>(IsoSpecTableViewColumns::ATOMIC_NO))
          .toInt(&ok);
      if(!ok)
        ++errors;

      mass = dataStringList.at(static_cast<int>(IsoSpecTableViewColumns::MASS))
               .toDouble(&ok);
      if(!ok)
        ++errors;

      massNo =
        dataStringList.at(static_cast<int>(IsoSpecTableViewColumns::MASS_NO))
          .toInt(&ok);
      if(!ok)
        ++errors;

      extraNeutrons =
        dataStringList
          .at(static_cast<int>(IsoSpecTableViewColumns::EXTRA_NEUTRONS))
          .toInt(&ok);
      if(!ok)
        ++errors;

      probability =
        dataStringList
          .at(static_cast<int>(IsoSpecTableViewColumns::PROBABILITY))
          .toDouble(&ok);
      if(!probability || !ok)
        ++errors;

      logProbability =
        dataStringList
          .at(static_cast<int>(IsoSpecTableViewColumns::LOG_PROBABILITY))
          .toDouble(&ok);
      if(!ok)
        ++errors;

      radioactive =
        dataStringList
          .at(static_cast<int>(IsoSpecTableViewColumns::RADIOACTIVE))
          .toInt(&ok);
      if(!ok)
        ++errors;

      if(errors)
        {
          message(QString("Failed to read line: %1").arg(line), 5000);
          return false;
        }

      Isotope *newIsotope = new Isotope(id,
                                        element,
                                        symbol,
                                        atomicNo,
                                        mass,
                                        massNo,
                                        extraNeutrons,
                                        probability,
                                        logProbability,
                                        radioactive ? true : false);

      // qDebug() << "entity:" << newEntity->asText();

      isoSpecEntityList.append(newIsotope);

      // Now construct the list of Atom instances that will be
      // required to work with the Formula.

      if(element != lastElement)
        {
          // We are starting a new Atom. Either this is the very
          // first atom or one other atom had been prepared. In that case we
          // need to store it in the list.

          if(atom != nullptr)
            {
              // Sanity check, we cannot be at first iteration.
              if(iter == 0)
                qFatal("Programming error.");

              // qDebug() << "Now appending the previously formed atom:"
              //<< atom->name();

              // This atom list is necessary because we work with
              // Formula and it need it.
              isoSpecAtomList.append(atom);
            }

          // Now start with the new atom.

          // qDebug() << "Now creating a new atom" << currentElement;

          atom = new Atom(element, symbol);

          isoSpecAtomList.append(atom);

          // Now feed it with the very first isotope.
          Isotope *isotope = new Isotope(mass, probability);

          // qDebug() << "Now appending isotope:" << isotope->massString()
          //<< "for atom" << atom->name();

          atom->appendIsotope(isotope);
        }
      else // same as if(currentElement == lastElement)
        {
          // We are still going on with the same alement, so add a new
          // isotope.
          Isotope *isotope = new Isotope(mass, probability);

          // qDebug() << "Now appending isotope:" << isotope->massString()
          //<< "for atom" << atom->name();

          atom->appendIsotope(isotope);
        }

      // Store the current element name so that at next iteration we can see
      // if we are still working on isotoes of the same element or starting a
      // new element.
      lastElement = element;

      // Store the iteration count because we need it for checks.
      ++iter;
    }
  // End of
  // while(!in.atEnd())

  // qDebug() << "Finished creating all the Isotope
  // instances.";

  file.close();

  // At this point, it seems that the loading went fine.

  // There might be old entities in the user table view/model, so first remove
  // everything and then add the new ones.

  // qDebug() << "Now removing the old entities";

  mpa_isoSpecStandardUserTableViewModel->removeIsoSpecEntities(0, -1);

  // qDebug() << "Now adding the new entities";

  mpa_isoSpecStandardUserTableViewModel->addIsoSpecEntities(isoSpecEntityList);

  // for(int iter = 0; iter < m_isoSpecEntityList.size(); ++iter)
  //{
  // Isotope *entity = m_isoSpecEntityList.at(iter);

  // qDebug() << "Entity:" << entity->asText();
  //}

  // Now that the entities stuff went flawlessly, let's do the same with the
  // Atom* instances.

  // But we first need to delete the preexisting Atom instances that
  // might have been created during a previous data load operation. Note that
  // we do not need to work on the entity list, because that it handled by the
  // removeIsoSpecEntities function in the model that was called above.

  // First clear (no delete) all the atoms form the list
  m_isoSpecStandardUserAtomList.clear();
  // Now clear them form the vector (will actuall free them)
  m_isoSpecStandardUserAtomVector.clear();

  // And now transfer the new Atom instances to the list/vector that
  // we have emptied at the moment.

  while(isoSpecAtomList.size())
    {
      Atom *atom = isoSpecAtomList.takeFirst();
      m_isoSpecStandardUserAtomList.append(atom);
      m_isoSpecStandardUserAtomVector.push_back(atom);
    }

  return true;
}


// The manual configuration that was saved can be reloaded and all the
// widgets will be recreated as necessary.
bool
IsoSpecDataHandler::loadUserManualConfiguration()
{
  //$ cat prova.txt
  //[atom]
  // symbol C count 100
  //[isotopes] 2
  // mass 12.000 prob 0.9899
  // mass 13.000 prob 0.010

  QString fileName = QFileDialog::getOpenFileName(
    this, tr("Load configuration"), QDir::home().absolutePath());

  if(fileName.isEmpty())
    {
      message("The file name to read from is empty!", 5000);
      return false;
    }

  QFile file(fileName);
  if(!file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
      message("Failed to open file for reading.", 5000);
      return false;
    }

  // At this point, make sure we reset the various member atomistic
  // data-storing.

  m_userManualAtomVector.clear();
  m_atomCountPairVector.clear();

  bool wasStartedOneAtom    = false;
  bool wasAtomLine          = false;
  bool wasSymbolCountLine   = false;
  bool wasIsotopesCountLine = false;
  bool wasMassProbLine      = false;

  QString symbol;
  int atomCount    = 0;
  int isotopeCount = 0;
  double mass      = 0;
  double prob      = 0;

  bool ok = false;

  QRegularExpression commentRegexp("^\\s+#");

  QRegularExpression symbolCountRegexp(
    "^\\s+symbol\\s+([A-Z][a-z]?)\\s+count\\s+(\\d+)");

  QRegularExpression isotopesRegexp("^\\s+\\[isotopes\\]\\s(\\d+)");

  // QRegularExpression massProbRegexp = QRegularExpression(
  //"^\\s+mass\\s+(\\d*\\.?\\d*[e]?[-]?[+]?\\d*)\\s+prob\\s+([^\\d^\\.^-]+)(-?"
  //"\\d*\\.?\\d*[e]?[-]?[+]?\\d*)");

  QRegularExpression massProbRegexp = QRegularExpression(
    "^\\s+mass\\s(\\d*\\.?\\d*[e]?[-]?[+]?\\d*)\\sprob\\s(\\d*\\.?\\d*[e]?["
    "-]"
    "?["
    "+]?\\d*)");

  // qDebug() << "The mass prob regexp is valid?" <<
  // massProbRegexp.isValid();

  //"\t\tmass 13.000000000000000000000000000000000000000000000000000000000000
  // prob 0.010000000000000000208166817117216851329430937767028808593750"

  // Instantiate an atom that we'll clear each time we push_back it to the
  // vector.
  Atom atom;

  QTextStream in(&file);
  while(!in.atEnd())
    {
      QString line = in.readLine();

      // Ignore empty or comment lines
      if(line.length() < 1)
        continue;

      QRegularExpressionMatch match = commentRegexp.match(line);
      if(match.hasMatch())
        continue;

      if(line == "[atom]")
        {
          if(wasStartedOneAtom && !wasMassProbLine)
            {
              qDebug() << "Error";
              return false;
            }

          if(wasStartedOneAtom)
            {
              // qDebug();
              // We are starting a new element configuration stanza, but in
              // fact
              // aother was already going on. We need to terminate it.

              // Sanity check

              if(isotopeCount != atom.isotopeList().size())
                {
                  qDebug() << "Error";
                  return false;
                }

              // Now we can be confident.

              m_userManualAtomVector.push_back(atom);

              // qDebug() << "The completed atom:" << atom.asText();

              // Clear the atom for next element configuration iteration.
              atom.clear();
            }

          wasStartedOneAtom = true;

          wasAtomLine          = true;
          wasSymbolCountLine   = false;
          wasIsotopesCountLine = false;
          wasMassProbLine      = false;
          continue;
        }

      // qDebug();

      match = symbolCountRegexp.match(line);
      if(match.hasMatch())
        {
          if(!wasAtomLine || wasSymbolCountLine)
            {
              qDebug() << "Error";
              return false;
            }

          symbol = match.captured(1);
          atom.setName(symbol);
          atom.setSymbol(symbol);

          atomCount = match.captured(2).toInt(&ok);
          if(!ok || !atomCount)
            {
              qDebug() << "Error";
              return false;
            }

          // Now that we have the symbol and the count, set the pair.

          std::pair<QString, int> atomCountPair(symbol, atomCount);
          m_atomCountPairVector.push_back(atomCountPair);

          // qDebug() << "Appended new pair:" << symbol << " " << atomCount;

          wasSymbolCountLine   = true;
          wasAtomLine          = false;
          wasIsotopesCountLine = false;
          wasMassProbLine      = false;

          continue;
        }

      // qDebug();
      match = isotopesRegexp.match(line);
      if(match.hasMatch())
        {
          if(!wasSymbolCountLine)
            {
              qDebug() << "Error";
              return false;
            }

          isotopeCount = match.captured(1).toInt(&ok);
          if(!ok || !isotopeCount)
            {
              qDebug() << "Error";
              return false;
            }

          // qDebug() << "The isotope count is:" << isotopeCount;

          wasIsotopesCountLine = true;
          wasSymbolCountLine   = false;
          wasAtomLine          = false;
          wasMassProbLine      = false;

          continue;
        }

      // qDebug();

      match = massProbRegexp.match(line);
      if(match.hasMatch())
        {
          // qDebug() << "The mass prob line match:" <<
          // match.capturedTexts();

          if(!wasIsotopesCountLine && !wasMassProbLine)
            {
              qDebug() << "Error";
              return false;
            }

          mass = match.captured(1).toDouble(&ok);
          if(!ok)
            {
              qDebug() << "Error";
              return false;
            }

          prob = match.captured(2).toDouble(&ok);
          if(!ok)
            {
              qDebug() << "Error";
              return false;
            }

          // At this point, we can allocate an isotope and append it to the
          // atom.

          atom.appendIsotope(new Isotope(mass, prob));

          // qDebug() << "The atom now is:" << atom.asText();

          wasMassProbLine      = true;
          wasIsotopesCountLine = false;
          wasSymbolCountLine   = false;
          wasAtomLine          = false;

          continue;
        }
    }

  // We have fnished iterating in the file's lines but we were parsing an
  // atom,
  // append it.

  // Sanity check
  if(!wasStartedOneAtom)
    {
      qDebug() << "Error: not a single atom could be parsed.";
      return false;
    }

  if(isotopeCount != atom.isotopeList().size())
    {
      qDebug() << "Error";
      return false;
    }

  // Now we can be confident.

  m_userManualAtomVector.push_back(atom);

  // Clear the atom for next element configuration iteration.
  atom.clear();

  // At this point, we have all the required data to start creating the
  // widgets!

  initializeIsoSpecManualConfigurationWidgets();

  return true;
}


bool
IsoSpecDataHandler::saveUserManualConfiguration()
{
  // We need to iterate into the various widgets, exactly as done for the
  // run of the manual configuration.

  // First run the validation which has also the side effect to set aside in
  // member variables the needed atomistic data to later run IsoSpec or
  // create the config string.

  std::size_t elementNumber = validateManualConfig();

  if(!elementNumber)
    {
      // The validation function creates all the necessary feedback.
      return false;
    }

  QString configuration;

  for(std::vector<std::pair<QString, int>>::iterator it =
        m_atomCountPairVector.begin();
      it != m_atomCountPairVector.end();
      ++it)
    {
      std::pair<QString, int> atomSymbolCount = *it;

      QString symbol = it->first;
      int atomCount  = it->second;

      configuration += QString(
                         "[atom]\n"
                         "\tsymbol %1 count %2\n")
                         .arg(symbol)
                         .arg(atomCount);

      // Get the matching Atom from the user-configured atom list.

      bool atomFound = false;

      for(std::vector<Atom>::iterator jt = m_userManualAtomVector.begin();
          jt != m_userManualAtomVector.end();
          ++jt)
        {
          Atom atom = *jt;

          if(atom.symbol() == symbol)
            {
              int isotopeCount = atom.isotopeList().size();

              configuration += QString("\t[isotopes] %1\n").arg(isotopeCount);

              for(int jter = 0; jter < isotopeCount; ++jter)
                {
                  Isotope *isotope = atom.isotopeList().at(jter);

                  configuration += QString("\t\tmass %1 prob %2\n")
                                     .arg(isotope->mass(), 0, 'f', 60)
                                     .arg(isotope->abundance(), 0, 'f', 60);
                }

              atomFound = true;
            }
          else
            continue;
        }
      // Finished searching for atom symbol in the list of user manual
      // configuration atom vector.

      if(!atomFound)
        qFatal("Programming error.");

      configuration += "\n";
    }
  // Finished iterating in the vector of all the symbols found in the
  // formula submitted by the user.

  // qDebug() << "Configuration:" << configuration;

  // At this point let the user choose a file for that config.

  QFileDialog fileDlg(this);
  fileDlg.setFileMode(QFileDialog::AnyFile);
  fileDlg.setAcceptMode(QFileDialog::AcceptSave);

  QString fileName = QFileDialog::getSaveFileName(
    this, tr("Save configuration"), QDir::home().absolutePath());

  if(!fileName.isEmpty())
    {

      QFile file(fileName);
      if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
        {
          message("Failed to open file for writing.", 5000);
          return false;
        }

      QTextStream out(&file);

      out << configuration;

      out.flush();

      file.close();
    }

  message(QString("Configuration saved in %1").arg(fileName), 5000);

  return true;
}


} // namespace libmass

} // namespace msxps


#if 0

Example from IsoSpec.

const int elementNumber = 2;
const int isotopeNumbers[2] = {2,3};

const int atomCounts[2] = {2,1};


const double hydrogen_masses[2] = {1.00782503207, 2.0141017778};
const double oxygen_masses[3] = {15.99491461956, 16.99913170, 17.9991610};

const double* isotope_masses[2] = {hydrogen_masses, oxygen_masses};

const double hydrogen_probs[2] = {0.5, 0.5};
const double oxygen_probs[3] = {0.5, 0.3, 0.2};

const double* probs[2] = {hydrogen_probs, oxygen_probs};

IsoLayeredGenerator iso(Iso(elementNumber, isotopeNumbers, atomCounts, isotope_masses, probs), 0.99);

#endif
