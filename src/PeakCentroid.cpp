/* BEGIN software license
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This file is part of the msXpertSuite project.
 *
 * The msXpertSuite project is the successor of the massXpert project. This
 * project now includes various independent modules:
 *
 * - massXpert, model polymer chemistries and simulate mass spectrometric data;
 * - mineXpert, a powerful TIC chromatogram/mass spectrum viewer/miner;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// Local includes
#include "PeakCentroid.hpp"
#include <ctime>


namespace msxps
{

namespace libmass
{


  PeakCentroid::PeakCentroid(double mz, double intensity)
  {
    Q_ASSERT(mz >= 0);
    Q_ASSERT(intensity >= 0);

    m_mz        = mz;
    m_intensity = intensity;
  }

  PeakCentroid::PeakCentroid() : m_mz(0), m_intensity(0)
  {
  }


  PeakCentroid::PeakCentroid(const PeakCentroid &other)
    : m_mz(other.m_mz), m_intensity(other.m_intensity)
  {
  }


  PeakCentroid::~PeakCentroid()
  {
  }


  PeakCentroid &
  PeakCentroid::operator=(const PeakCentroid &other)
  {
    if(&other == this)
      return *this;

    m_mz        = other.m_mz;
    m_intensity = other.m_intensity;

    return *this;
  }

  bool
  PeakCentroid::operator==(const PeakCentroid &other)
  {
    return (m_mz == other.m_mz && m_intensity == other.m_intensity);
  }

 bool
  PeakCentroid::operator!=(const PeakCentroid &other)
  {
    return (m_mz != other.m_mz || m_intensity != other.m_intensity);
  }

  void
  PeakCentroid::setMz(double value)
  {
    m_mz = value;
  }


  double
  PeakCentroid::mz() const
  {
    return m_mz;
  }


  void
  PeakCentroid::setIntensity(double value)
  {
    Q_ASSERT(value >= 0);

    m_intensity = value;
  }


  void
  PeakCentroid::incrementIntensity(double value)
  {
    Q_ASSERT(value >= 0);

    m_intensity += value;
  }


  double
  PeakCentroid::intensity() const
  {
    return m_intensity;
  }


  QString
  PeakCentroid::intensity(int decimalPlaces) const
  {
    if(decimalPlaces < 0)
      decimalPlaces = 0;

    QString prob;

    prob.setNum(m_intensity, 'f', decimalPlaces);

    return prob;
  }


  QString
  PeakCentroid::toString() const
  {
    QString text = QString("m/z: %1, int: %2)\n").arg(m_mz).arg(m_intensity);

    return text;
  }


} // namespace libmass

} // namespace msxps
