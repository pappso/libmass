# This is a convenience file to use make from vim
# from wherever in the source code directories.
all: 
	cd ../../build-area/unix && make -j20

.PHONY: clean
clean:
	cd ../../build-area/unix && make clean


